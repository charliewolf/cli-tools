crw's CLI tools:
   This is a collection of tools I use in my day to day life/work.


LICENSE:
---
Since most of these are mine (e.g. search, r, otp, show-alerts) but some of these (e.g. mutt-ical, gpgedit) are forks, various licenses apply. Specific license info is included in files and subdirectories.

Where not otherwise indicated, tools are licensed under my standard Tequilaware license located at LICENSE.txt in the root of this repository.

THE TOOLS
---
* search
  - An interactive CLI search client for DuckDuckGo using Tor.
* r
  - CLI HTTP request/response analyzer -- useful for quickly testing things 
* otp
  - Google-authenticator one-time passwords backed by gpg (it is highly recommended to use a smartcard with this)
* gpgedit
  - Edit encrypted files without ever decrypting to disk
* mutt-ical
  - Accept/decline calendar invitations using mutt
* show-alerts
  - A cron job that alerts me in my terminal of upcoming events in my calendar
* starbucks-captive-portal
  - Automatically connect through the 'Google Starbucks' captive portal available at most US Starbucks alocations (bypassing your default DNS settings if you're like me and are piping DNS through a VPN that won't be connected yet when running this)
* shapeshift
  - Tor CLI client for shapeshift.io
* extract-links
  - Extract links from HTML emails
* yubikey
  - GPG smartcard-based yubikey compatible implementation
* sort-csv
  - Sorts CSV files by column
* explicit-arch-packages
  - Shows explicitly installed arch linux packages not in base or base-devel (useful for config managing and existing system)
