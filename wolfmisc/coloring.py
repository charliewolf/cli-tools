import sys


def color_if_tty(style, *print_args, **print_kwargs):
    # This uses colorama to color output but only if the output is a terminal

    output = print_kwargs.get('file', sys.stdout)
    new_print_args = list(print_args)
    if output.isatty():
        new_print_args[0] = style + print_args[0]
    return print(*new_print_args, **print_kwargs)
