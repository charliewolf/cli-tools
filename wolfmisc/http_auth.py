from requests.auth import AuthBase


class HTTPBearerAuth(AuthBase):
    """ Attaches HTTP Bearer token to a request """

    def __init__(self, token):
        self.token = token

    def __eq__(self, other):
        return self.token == other.token

    def __call__(self, r):
        r.headers['Authorization'] = 'Bearer %s' % self.token
        return r
