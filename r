#!/usr/bin/env python

# r - a cli tool for inspecting http requests

import json
import os
import sys

import colorama
import requests

from colorama import Style, Fore
from urllib.parse import urlparse, urlunparse
from requests_toolbelt.adapters import host_header_ssl
from wolfmisc.coloring import color_if_tty as cprint
from wolfmisc.http_auth import HTTPBearerAuth
from requests.auth import HTTPBasicAuth

def main(args):
    session = requests.Session()

    ip = None

    # A poor man's argparse
    if len(args) < 2:
        usage = "Usage: {} [method] <url> [ip]".format(*args)
        raise RuntimeError(usage)
    try:
        method, url = args[1:3]
    except ValueError:
        method, url = "GET", args[1]
    try:
        ip = args[3]
    except IndexError:
        pass

    parts = urlparse(url, scheme='http', allow_fragments=False)
    if not parts.netloc:
        parts = urlparse('//%s' % url, scheme='http', allow_fragments=False)

    auth = None

    headers = {}
    headers['Host'] = parts.netloc
    headers['User-Agent'] = os.environ.get('USER_AGENT', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.85 Safari/537.36')  # The most common user agent

    if 'BEARER_TOKEN' in os.environ:
        auth = HTTPBearerAuth(os.environ['BEARER_TOKEN'])
    elif 'BASIC_AUTH' in os.environ:
        try:
            username, password = os.environ['BASIC_AUTH'].split(':')
        except ValueError:
            username, password = os.environ['BASIC_AUTH'], ''
        auth = HTTPBasicAuth(username, password)

    scheme = parts.scheme

    if ip and ip.startswith('http'):
        ip_parts = urlparse(ip)
        ip = ip_parts.netloc
        scheme = ip_parts.scheme

    if ip and scheme == 'https':
        session.mount('https://', host_header_ssl.HostHeaderSSLAdapter())

    data = None
    if method.upper() in ('POST', 'PUT', 'PATCH'):
        data = sys.stdin.read()

    if not data:
        data = None  # Don't bother sending an empty string

    try:
        json.loads(data)
        headers['Content-Type'] = 'application/json'
        headers['Accept'] = 'application/json'
    except (TypeError, json.decoder.JSONDecodeError):
        pass

    new_parts = scheme, ip or parts.netloc, parts.path, parts.params, parts.query, ''
    new_url = urlunparse(new_parts)
    response = session.request(method.upper(), new_url, headers=headers, allow_redirects=False, auth=auth, data=data)
    request = response.request

    colorama.init(autoreset=True)
    cprint(Style.BRIGHT + Fore.BLUE, "REQUEST:", file=sys.stderr)
    cprint(Fore.CYAN, f"    {request.method} {request.url}", file=sys.stderr)
    print("", file=sys.stderr)

    for header, value in request.headers.items():
        cprint(Fore.BLUE, f"    {header}: {value}", file=sys.stderr)

    print("", file=sys.stderr)

    cprint(Style.BRIGHT + Fore.RED, "RESPONSE:", file=sys.stderr)
    print(Fore.MAGENTA + f"    {response.status_code} {response.reason}", file=sys.stderr)
    print("", file=sys.stderr)
    for header, value in response.headers.items():
        print(Fore.RED + f"    {header}: {value}", file=sys.stderr)

    print("", file=sys.stderr)
    final_output = response.text
    try:
        json_data = json.loads(response.text)
        final_output = str(json.dumps(json_data, indent=4))
    except json.decoder.JSONDecodeError:
        pass

    cprint(Fore.WHITE, final_output)


if __name__ == '__main__':
    try:
        sys.exit(main(args=sys.argv) or 0)
    except (RuntimeError,) as exc:
        print(*exc.args, file=sys.stderr)
        try:
            sys.exit(exc.exit_code)
        except AttributeError:
            sys.exit(-1)
